#include "candidato.hpp"
#include <string>
#include <fstream>
#include <vector>
#include <iostream>

Candidato::Candidato(){
    votos= 0;
}
Candidato::Candidato(string descricao_cargo,string numero_candidato,string nome_urna_candidato,string numero_partido,string sigla_partido,string nome_partido){
  
  this->descricao_cargo=descricao_cargo;
  this->numero_candidato=numero_candidato;
  this->nome_urna_candidato=nome_urna_candidato;
  this->numero_partido=numero_partido;
  this->sigla_partido=sigla_partido;
  this->nome_partido=nome_partido;
  
}
Candidato::~Candidato(){}
  
string Candidato::get_descricao_cargo(){
    return descricao_cargo;
}
void Candidato::set_descricao_cargo(string descricao_cargo){
    this->descricao_cargo=descricao_cargo;
}
string Candidato::get_numero_candidato(){
    return numero_candidato;
}
void Candidato::set_numero_candidato(string numero_candidato){
    this->numero_candidato=numero_candidato;
}
string Candidato::get_nome_urna_candidato(){
    return nome_urna_candidato;
}
void Candidato::set_nome_urna_candidato(string nome_urna_candidato){
    this->nome_urna_candidato=nome_urna_candidato;
}
string Candidato::get_numero_partido(){
    return numero_partido;
}
void Candidato::set_numero_partido(string numero_partido){
    this->numero_partido=numero_partido;
}
string Candidato::get_sigla_partido(){
    return sigla_partido;
}
void Candidato::set_sigla_partido(string sigla_partido){
    this->sigla_partido=sigla_partido;
}
string Candidato::get_nome_partido(){
    return nome_partido;
}
void Candidato::set_nome_partido(string nome_partido){
    this->nome_partido=nome_partido;
}
