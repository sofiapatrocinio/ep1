#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include "candidato.hpp"
#include "leitura.hpp"
#include "presidente.hpp"
#include "governador.hpp"
#include "senador.hpp"

using namespace std;

Leitura::Leitura() {}
Leitura::~Leitura() {}

vector<Candidato> Leitura::armazenar_candidatosdf()
{
    string descricao_cargo;
    string numero_candidato;
    string nome_urna_candidato;
    string numero_partido;
    string sigla_partido;
    string nome_partido;
    vector<Candidato> candidatos;

    ifstream file("data/consulta_cand_2018_DF.csv");

    if (file.is_open())
    {

        for (int i = 0; i < 1238; i++)
        {
            getline(file, descricao_cargo, ',');
            getline(file, numero_candidato, ',');
            getline(file, nome_urna_candidato, ',');
            getline(file, numero_partido, ',');
            getline(file, sigla_partido, ',');
            getline(file, nome_partido, '\n');
            Candidato candidato = Candidato(descricao_cargo, numero_candidato, nome_urna_candidato, numero_partido, sigla_partido, nome_partido);
            candidatos.push_back(candidato);
        }
    }
    file.close();
    return candidatos;
}
vector<Governador> Leitura::armazenar_governadores()
{
    string descricao_cargo;
    string numero_candidato;
    string nome_urna_candidato;
    string numero_partido;
    string sigla_partido;
    string nome_partido;
    vector<Candidato> vices;
    vector<Governador> governadores;

    ifstream file("data/consulta_cand_2018_DF.csv");

    if (file.is_open())
    {

        for (int i = 0; i < 1238; i++)
        {
            getline(file, descricao_cargo, ',');
            getline(file, numero_candidato, ',');
            getline(file, nome_urna_candidato, ',');
            getline(file, numero_partido, ',');
            getline(file, sigla_partido, ',');
            getline(file, nome_partido, '\n');
            if (descricao_cargo == "GOVERNADOR")
            {
                Governador governador = Governador(descricao_cargo, numero_candidato, nome_urna_candidato, numero_partido, sigla_partido, nome_partido);
                governadores.push_back(governador);
            }
            else if (descricao_cargo == "VICE-GOVERNADOR")
            {
                Candidato vice = Candidato(descricao_cargo, numero_candidato, nome_urna_candidato, numero_partido, sigla_partido, nome_partido);
                vices.push_back(vice);
            }
        }
        for (unsigned int m = 0; m < governadores.size(); m++)
        {
            string numero_candidato;
            numero_candidato = governadores[m].get_numero_candidato();
            for (unsigned int n = 0; n < vices.size(); n++)
            {
                if (vices[n].get_numero_candidato() == numero_candidato)
                {
                    governadores[m].set_vice(vices[n].get_nome_urna_candidato());
                }
            }
        }
    }
    file.close();
    return governadores;
}

vector<Senador> Leitura::armazenar_senadores()
{
    string descricao_cargo;
    string numero_candidato;
    string nome_urna_candidato;
    string numero_partido;
    string sigla_partido;
    string nome_partido;
    vector<Candidato> suplentes1;
    vector<Candidato> suplentes2;
    vector<Senador> senadores;

    ifstream file("data/consulta_cand_2018_DF.csv");

    if (file.is_open())
    {

        for (int i = 0; i < 1238; i++)
        {
            getline(file, descricao_cargo, ',');
            getline(file, numero_candidato, ',');
            getline(file, nome_urna_candidato, ',');
            getline(file, numero_partido, ',');
            getline(file, sigla_partido, ',');
            getline(file, nome_partido, '\n');
            if (descricao_cargo == "SENADOR")
            {
                Senador senador = Senador(descricao_cargo, numero_candidato, nome_urna_candidato, numero_partido, sigla_partido, nome_partido);
                senadores.push_back(senador);
            }
            else if (descricao_cargo == "PRIMEIRO SUPLENTE")
            {
                Candidato suplente1 = Candidato(descricao_cargo, numero_candidato, nome_urna_candidato, numero_partido, sigla_partido, nome_partido);
                suplentes1.push_back(suplente1);
            }
            else if (descricao_cargo == "SEGUNDO SUPLENTE")
            {
                Candidato suplente2 = Candidato(descricao_cargo, numero_candidato, nome_urna_candidato, numero_partido, sigla_partido, nome_partido);
                suplentes2.push_back(suplente2);
            }
        }
        for (unsigned int m = 0; m < senadores.size(); m++)
        {
            string numero_candidato;
            numero_candidato = senadores[m].get_numero_candidato();
            for (unsigned int n = 0; n < suplentes1.size(); n++)
            {
                if (suplentes1[n].get_numero_candidato() == numero_candidato)
                {
                    senadores[m].set_suplente1(suplentes1[n].get_nome_urna_candidato());
                }
            }
            for (unsigned int n = 0; n < suplentes2.size(); n++)
            {
                if (suplentes2[n].get_numero_candidato() == numero_candidato)
                {
                    senadores[m].set_suplente2(suplentes2[n].get_nome_urna_candidato());
                }
            }
        }
    }
    file.close();
    return senadores;
}

vector<Presidente> Leitura::armazenar_candidatosbr()
{
    string descricao_cargo;
    string numero_candidato;
    string nome_urna_candidato;
    string numero_partido;
    string sigla_partido;
    string nome_partido;
    vector<Candidato> vices;
    vector<Presidente> presidentes;

    ifstream file1("data/consulta_cand_2018_BR.csv");

    if (file1.is_open())
    {

        for (unsigned int k = 0; k < 26; k++)
        {
            getline(file1, descricao_cargo, ',');
            getline(file1, numero_candidato, ',');
            getline(file1, nome_urna_candidato, ',');
            getline(file1, numero_partido, ',');
            getline(file1, sigla_partido, ',');
            getline(file1, nome_partido, '\n');
            if (descricao_cargo == "PRESIDENTE")
            {
                Presidente presidente = Presidente(descricao_cargo, numero_candidato, nome_urna_candidato, numero_partido, sigla_partido, nome_partido);
                presidentes.push_back(presidente);
            }
            else if (descricao_cargo == "VICE-PRESIDENTE")
            {
                Candidato vice = Candidato(descricao_cargo, numero_candidato, nome_urna_candidato, numero_partido, sigla_partido, nome_partido);
                vices.push_back(vice);
            }
        }

        for (unsigned int m = 0; m < presidentes.size(); m++)
        {
            string numero_candidato;
            numero_candidato = presidentes[m].get_numero_candidato();
            for (unsigned int n = 0; n < vices.size(); n++)
            {
                if (vices[n].get_numero_candidato() == numero_candidato)
                {
                    presidentes[m].set_vice(vices[n].get_nome_urna_candidato());
                }
            }
        }
    }

    file1.close();
    return presidentes;
}
