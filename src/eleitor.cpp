#include "eleitor.hpp"
#include "candidato.hpp"
#include "presidente.hpp"
#include "leitura.hpp"
#include <string>
#include <fstream>
#include <vector>
#include <iostream>

using namespace std;

Eleitor::Eleitor(string nome_eleitor, string voto_presidente, string voto_governador, string voto_senador1, string voto_senador2, string voto_depu_federal,string voto_depu_distrital)
{
    this->nome_eleitor = nome_eleitor;
    this->voto_presidente = voto_presidente;
    this->voto_governador = voto_governador;
    this->voto_senador1 = voto_senador1;
    this->voto_senador2 = voto_senador2;
    this->voto_depu_federal = voto_depu_federal;
    this->voto_depu_distrital = voto_depu_distrital;
}
Eleitor::~Eleitor() {}
string Eleitor::get_nome_eleitor()
{
    return nome_eleitor;
}
void Eleitor::set_nome_eleitor(string nome_eleitor)
{
    this->nome_eleitor = nome_eleitor;
}
string Eleitor::get_voto_presidente()
{
    return voto_presidente;
}
void Eleitor::set_voto_presidente(string voto_presidente)
{
    this->voto_presidente = voto_presidente;
}
string Eleitor::get_voto_senador1()
{
    return voto_senador1;
}
void Eleitor::set_voto_senador1(string voto_senador1)
{
    this->voto_senador1 = voto_senador1;
}
string Eleitor::get_voto_senador2()
{
    return voto_senador2;
}
void Eleitor::set_voto_senador2(string voto_senador2)
{
    this->voto_senador2 = voto_senador2;
}
string Eleitor::get_voto_governador()
{
    return voto_governador;
}
void Eleitor::set_voto_governador(string voto_governador)
{
    this->voto_governador = voto_governador;
}
string Eleitor::get_voto_depu_federal()
{
    return voto_depu_federal;
}
void Eleitor::set_voto_depu_federal(string voto_depu_federal)
{
    this->voto_depu_federal = voto_depu_federal;
}
string Eleitor::get_voto_depu_distrital()
{
    return voto_depu_distrital;
}
void Eleitor::set_voto_depu_distrital(string voto_depu_distrital)
{
    this->voto_depu_distrital = voto_depu_distrital;
}


