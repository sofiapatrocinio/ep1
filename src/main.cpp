#include <iostream>
#include "candidato.hpp"
#include "leitura.hpp"
#include "presidente.hpp"
#include "eleitor.hpp"
#include <vector>
#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

int main()
{

    Leitura leitura = Leitura();

    vector<Presidente> candidatos_br = leitura.armazenar_candidatosbr();
    vector<Candidato> candidatos_df = leitura.armazenar_candidatosdf();
    vector<Governador> governadores = leitura.armazenar_governadores();
    vector<Senador> senadores = leitura.armazenar_senadores();

    vector<Eleitor> eleitores;
    string nome;
    string presidente;
    string governador;
    string senador1;
    string senador2;
    string depu_federal;
    string depu_distrital;
    string decisao;

    string nome_eleitor;
    string voto_presidente;
    string voto_governador;
    string voto_senador1;
    string voto_senador2;
    string voto_depu_federal;
    string voto_depu_distrital;
    string vencedor_br, vencedor_gov, vencedor_s1, vencedor_df, vencedor_dd;

    int numero_eleitores;
    int presidente_inicial = 0;
    int governador_inicial = 0;
    int senador1_inicial = 0;
    int senador2_inicial = 0;
    int depu_federal_inicial = 0;
    int depu_distrital_inicial = 0;
    int pres = 0, gov = 0, s1 = 0, s2 = 0, df = 0, dd = 0;
    int maior_br = 0, maior_gov = 0, maior_s1 = 0, maior_df = 0, maior_dd = 0;

QNT:
    cout << "Quantidade de eleitores: " << endl;
    cin >> numero_eleitores;

    if (numero_eleitores < 0)
    {
        cout << "Quantidade inválida." << endl;
        goto QNT;
    }

    for (int aux = 0; aux < numero_eleitores; aux++)
    {
        cout << "Nome eleitor: " << endl;
        cin.ignore();
        getline(cin, nome);

        system("clear");

        cout << "Presidente: " << endl
             << "Digite 0 para votar nulo." << endl
             << "Digite  'B' para votar em branco." << endl
             << "Número do candidato: ";

        cin >> presidente;

        if (presidente == "0")
        {
            cout << "Voto nulo." << endl
                 << "Digite 1 para confirmar voto nulo." << endl
                 << "Digite 0 para corrigir." << endl;
        DEC1:
            cin >> decisao;
            if (decisao == "1")
            {
                cout << cout << "Voto nulo computado!" << endl;
            }
            else if (decisao == "0")
                continue;
            else
            {
                cout << "Insira uma opção válida." << endl;
                goto DEC1;
            }
        }
        else if (presidente == "B" || presidente == "b")
        {
            cout << "Voto em branco." << endl
                 << "Digite 1 para confirmar voto em branco." << endl
                 << "Digite 0 para corrigir." << endl;
        DEC2:
            cin >> decisao;
            if (decisao == "1")
            {
                cout << "Voto em branco computado" << endl;
                presidente_inicial++;
            }
            else if (decisao == "0")
                continue;
            else
            {
                cout << "Insira uma opção válida." << endl;
                goto DEC2;
            }
        }
        else
        {
            for (unsigned int i = 0; i < candidatos_br.size(); i++)
            {
                if (presidente == candidatos_br[i].get_numero_candidato())
                {
                    cout << "Nome: " << candidatos_br[i].get_nome_urna_candidato() << endl;
                    cout << "Vice: " << candidatos_br[i].get_vice() << endl;
                    cout << "Sigla Partido: " << candidatos_br[i].get_sigla_partido() << endl;
                    cout << "Nome Partido: " << candidatos_br[i].get_nome_partido() << endl;
                    
                    cout << "Digite 1 para confirmar." << endl
                         << "Digite 0 para corrigir." << endl;
                DEK1:
                    cin >> decisao;
                    if (decisao == "1")
                    {
                        cout << "Voto computado!" << endl;
                        candidatos_br[i].votos++;
                        pres = i;
                    }
                    else if (decisao == "0")
                        continue;
                    else
                    {
                        cout << "Insira uma opção válida." << endl;
                        goto DEK1;
                    }
                    break;
                }
            }
        }

        cout << "Governador: " << endl
             << "Digite 0 para votar nulo." << endl
             << "Digite  'B' para votar em branco." << endl
             << "Número do candidato: ";

        cin >> governador;

        if (governador == "0")
        {
            cout << "Voto nulo." << endl
                 << "Digite 1 para confirmar voto nulo." << endl
                 << "Digite 0 para corrigir." << endl;
        DEC3:
            cin >> decisao;
            if (decisao == "1")
            {
                cout << "Voto nulo computado!" << endl;
            }
            else if (decisao == "0")
                continue;
            else
            {
                cout << "Insira uma opção válida." << endl;
                goto DEC3;
            }
        }
        else if (governador == "B" || governador == "b")
        {
            cout << "Voto em branco." << endl
                 << "Digite 1 para confirmar voto em branco." << endl
                 << "Digite 0 para corrigir." << endl;
        DEC4:
            cin >> decisao;
            if (decisao == "1")
            {
                cout << "Voto em branco computado!" << endl;
                governador_inicial++;
            }
            else if (decisao == "0")
                continue;
            else
            {
                cout << "Insira uma opção válida." << endl;
                goto DEC4;
            }
        }
        else
        {
            for (unsigned int i = 0; i < governadores.size(); i++)
            {
                if (governador == governadores[i].get_numero_candidato())
                {
                    cout << "Nome: " << governadores[i].get_nome_urna_candidato() << endl;
                    cout << "Vice: " << governadores[i].get_vice() << endl;
                    cout << "Sigla Partido: " << governadores[i].get_sigla_partido() << endl;
                    cout << "Nome Partido: " << governadores[i].get_nome_partido() << endl;
                    cout << "Digite 1 para confirmar." << endl
                         << "Digite 0 para corrigir." << endl;
                DEK2:
                    cin >> decisao;
                    if (decisao == "1")
                    {
                        cout << "Voto computado!" << endl;
                        governadores[i].votos++;
                        gov = i;
                    }
                    else if (decisao == "0")
                        continue;
                    else
                    {
                        cout << "Insira uma opção válida." << endl;
                        goto DEK2;
                    }
                    break;
                }
            }
        }

        cout << "Senador - 1ª vaga: " << endl
             << "Digite 0 para votar nulo." << endl
             << "Digite  'B' para votar em branco." << endl
             << "Número do candidato: ";

        cin >> senador1;

        if (senador1 == "0")
        {
            cout << "Voto nulo." << endl
                 << "Digite 1 para confirmar voto nulo." << endl
                 << "Digite 0 para corrigir." << endl;
        DEC5:
            cin >> decisao;
            if (decisao == "1")
            {
                cout << "Voto nulo computado!" << endl;
            }
            else if (decisao == "0")
                continue;
            else
            {
                cout << "Insira uma opção válida." << endl;
                goto DEC5;
            }
        }
        else if (senador1 == "B" || senador1 == "b")
        {
            cout << "Voto em branco." << endl
                 << "Digite 1 para confirmar voto em branco." << endl
                 << "Digite 0 para corrigir." << endl;
        DEC6:
            cin >> decisao;
            if (decisao == "1")
            {
                cout << "Voto em branco computado!" << endl;
                senador1_inicial++;
            }
            else if (decisao == "0")
                continue;
            else
            {
                cout << "Insira uma opção válida." << endl;
                goto DEC6;
            }
        }
        else
        {
            for (unsigned int i = 0; i < senadores.size(); i++)
            {
                if (senador1 == senadores[i].get_numero_candidato())
                {

                    cout << "Nome: " << senadores[i].get_nome_urna_candidato() << endl;
                    cout << "Sigla Partido: " << senadores[i].get_sigla_partido() << endl;
                    cout << "Nome Partido: " << senadores[i].get_nome_partido() << endl;
                    cout << "Primeiro Suplente: " << senadores[i].get_suplente1() << endl;
                    cout << "Segundo Suplente: " << senadores[i].get_suplente2() << endl;
                    cout << "Digite 1 para confirmar." << endl
                         << "Digite 0 para corrigir." << endl;
                DEK3:
                    cin >> decisao;
                    if (decisao == "1")
                    {
                        cout << "Voto computado!" << endl;
                        senadores[i].votos++;
                        s1 = i;
                    }
                    else if (decisao == "0")
                        continue;
                    else
                    {
                        cout << "Insira uma opção válida." << endl;
                        goto DEK3;
                    }
                    break;
                }
            }
        }

        cout << "Senador - 2ª vaga: " << endl
             << "Digite 0 para votar nulo." << endl
             << "Digite  'B' para votar em branco." << endl
             << "Número do candidato: ";

        cin >> senador2;

        if (senador2 == "0")
        {
            cout << "Voto nulo." << endl
                 << "Digite 1 para confirmar voto nulo." << endl
                 << "Digite 0 para corrigir." << endl;
        DEC7:
            cin >> decisao;
            if (decisao == "1")
            {
                cout << "Voto nulo computado!" << endl;
            }
            else if (decisao == "0")
                continue;
            else
            {
                cout << "Insira uma opção válida." << endl;
                goto DEC7;
            }
        }
        else if (senador2 == "B" || senador2 == "b")
        {
            cout << "Voto em branco." << endl
                 << "Digite 1 para confirmar voto em branco." << endl
                 << "Digite 0 para corrigir." << endl;
        DEC8:
            cin >> decisao;
            if (decisao == "1")
            {
                cout << "Voto em branco computado!" << endl;
                senador2_inicial++;
            }
            else if (decisao == "0")
                continue;
            else
            {
                cout << "Insira uma opção válida." << endl;
                goto DEC8;
            }
        }
        else
        {
            for (unsigned int i = 0; i < senadores.size(); i++)
            {
                if (senador2 == senadores[i].get_numero_candidato())
                {
                    cout << "Nome: " << senadores[i].get_nome_urna_candidato() << endl;
                    cout << "Sigla Partido: " << senadores[i].get_sigla_partido() << endl;
                    cout << "Nome Partido: " << senadores[i].get_nome_partido() << endl;
                    cout << "Primeiro Suplente: " << senadores[i].get_suplente1() << endl;
                    cout << "Segundo Suplente: " << senadores[i].get_suplente2() << endl;
                    cout << "Digite 1 para confirmar." << endl
                         << "Digite 0 para corrigir." << endl;
                    DEK6:
                    cin >> decisao;
                    if (decisao == "1")
                    {
                        cout << "Voto computado!" << endl;
                        senadores[i].votos++;
                        s2 = i;
                    }
                    else if (decisao == "0")
                        continue;
                    else
                    {
                        cout << "Insira uma opção válida." << endl;
                        goto DEK6;
                    }
                    break;
                }
            }
        }

        cout << "Deputado Federal: " << endl
             << "Digite 0 para votar nulo." << endl
             << "Digite  'B' para votar em branco." << endl
             << "Número do candidato: ";

        cin >> depu_federal;

        if (depu_federal == "0")
        {
            cout << "Voto nulo." << endl
                 << "Digite 1 para confirmar voto nulo." << endl
                 << "Digite 0 para corrigir." << endl;
        DEC9:
            cin >> decisao;
            if (decisao == "1")
                cout << "Voto nulo computado!" << endl;
            else if (decisao == "0")
                continue;
            else
            {
                cout << "Insira uma opção válida." << endl;
                goto DEC9;
            }
        }
        else if (depu_federal == "B" || depu_federal == "b")
        {
            cout << "Voto em branco." << endl
                 << "Digite 1 para confirmar voto em branco." << endl
                 << "Digite 0 para corrigir." << endl;
        DEC10:
            cin >> decisao;
            if (decisao == "1")
            {
                cout << "Voto em branco computado!" << endl;
                depu_federal_inicial++;
            }
            else if (decisao == "0")
                continue;
            else
            {
                cout << "Insira uma opção válida." << endl;
                goto DEC10;
            }
        }
        else
        {
            for (unsigned int i = 0; i < candidatos_df.size(); i++)
            {
                if (depu_federal == candidatos_df[i].get_numero_candidato() && candidatos_df[i].get_descricao_cargo() == "DEPUTADO FEDERAL")
                {
                    cout << "Nome: " << candidatos_df[i].get_nome_urna_candidato() << endl;
                    cout << "Sigla Partido: " << candidatos_df[i].get_sigla_partido() << endl;
                    cout << "Nome Partido: " << candidatos_df[i].get_nome_partido() << endl;
                    cout << "Digite 1 para confirmar." << endl
                         << "Digite 0 para corrigir." << endl;
                    DEK4:
                    cin >> decisao;
                    if (decisao == "1")
                    {
                        cout << "Voto computado!" << endl;
                        candidatos_df[i].votos++;
                        df = i;
                    }
                    else if (decisao == "0")
                        continue;
                    else
                    {
                        cout << "Insira uma opção válida." << endl;
                        goto DEK4;
                    }
                    break;
                }
            }
        }

        cout << "Deputado Distrital: " << endl
             << "Digite 0 para votar nulo." << endl
             << "Digite  'B' para votar em branco." << endl
             << "Número do candidato: ";

        cin >> depu_distrital;

        if (depu_distrital == "0")
        {
            cout << "Voto nulo." << endl
                 << "Digite 1 para confirmar voto nulo." << endl
                 << "Digite 0 para corrigir." << endl;
        DEC11:
            cin >> decisao;
            if (decisao == "1")
            {
                cout << "Voto nulo computado!" << endl;
            }
            else if (decisao == "0")
                continue;
            else
            {
                cout << "Insira uma opção válida." << endl;
                goto DEC11;
            }
        }
        else if (depu_distrital == "B" || depu_distrital == "b")
        {
            cout << "Voto em branco." << endl
                 << "Digite 1 para confirmar voto em branco." << endl
                 << "Digite 0 para corrigir." << endl;
        DEC12:
            cin >> decisao;
            ;
            if (decisao == "1")
            {
                cout << "Voto em branco computado!" << endl;
                depu_distrital_inicial++;
            }
            else if (decisao == "0")
                continue;
            else
            {
                cout << "Insira uma opção válida." << endl;
                goto DEC12;
            }
        }
        else
        {
            for (unsigned int i = 0; i < candidatos_df.size(); i++)
            {
                if (depu_distrital == candidatos_df[i].get_numero_candidato() && candidatos_df[i].get_descricao_cargo() == "DEPUTADO DISTRITAL")
                {

                    cout << "Nome: " << candidatos_df[i].get_nome_urna_candidato() << endl;
                    cout << "Sigla Partido: " << candidatos_df[i].get_sigla_partido() << endl;
                    cout << "Nome Partido: " << candidatos_df[i].get_nome_partido() << endl;
                    cout << "Digite 1 para confirmar." << endl
                         << "Digite 0 para corrigir." << endl;
                         DEK5:
                    cin >> decisao;
                    if (decisao == "1")
                    {
                        cout << "Voto computado!" << endl;
                        candidatos_df[i].votos++;
                        dd = i;
                    }
                    else if (decisao == "0")
                        continue;
                    else
                    {
                        cout << "Insira uma opção válida." << endl;
                        goto DEK5;
                    }
                    break;

                }
            }
        }

        Eleitor eleitorado = Eleitor(nome, candidatos_br[pres].get_nome_urna_candidato(), governadores[gov].get_nome_urna_candidato(), senadores[s1].get_nome_urna_candidato(), senadores[s2].get_nome_urna_candidato(), candidatos_df[df].get_nome_urna_candidato(), candidatos_df[dd].get_nome_urna_candidato());
        eleitores.push_back(eleitorado);
        cout << endl;
    }

    cout << "Relatório Eleição: " << endl;
    for (int i = 0; i < numero_eleitores; i++)
    {
        cout << "Nome: " << eleitores[i].get_nome_eleitor() << endl;
        cout << "Voto para presidente: " << eleitores[i].get_voto_presidente() << endl;
        cout << "Voto para governador: " << eleitores[i].get_voto_governador() << endl;
        cout << "Voto para senador(1º vaga): " << eleitores[i].get_voto_senador1() << endl;
        cout << "Voto para senador(2º vaga): " << eleitores[i].get_voto_senador2() << endl;
        cout << "Voto para deputado federal: " << eleitores[i].get_voto_depu_federal() << endl;
        cout << "Voto para deputado distrital: " << eleitores[i].get_voto_depu_distrital() << endl;
    }
    for (unsigned int i = 0; i < candidatos_br.size(); i++)
    {
        if (candidatos_br[i].votos > maior_br)
        {
            maior_br = candidatos_br[i].votos;
            vencedor_br = candidatos_br[i].get_nome_urna_candidato();
        }
    }

    for (unsigned int i = 0; i < governadores.size(); i++)
    {
        if (governadores[i].votos > maior_gov)
        {
            maior_gov = governadores[i].votos;
            vencedor_gov = governadores[i].get_nome_urna_candidato();
        }
    }

    for (unsigned int i = 0; i < senadores.size(); i++)
    {
        if (senadores[i].votos > maior_s1)
        {
            maior_s1 = senadores[i].votos;
            vencedor_s1 = senadores[i].get_nome_urna_candidato();
        }
    }

    for (unsigned int i = 0; i < candidatos_df.size(); i++)
    {
        if (candidatos_df[i].get_descricao_cargo() == "DEPUTADO FEDERAL")
        {
            if (candidatos_df[i].votos > maior_df)
            {
                maior_df = candidatos_df[i].votos;
                vencedor_df = candidatos_df[i].get_nome_urna_candidato();
            }
        }
    }

    for (unsigned int i = 0; i < candidatos_df.size(); i++)
    {
        if (candidatos_df[i].get_descricao_cargo() == "DEPUTADO DISTRITAL")
        {
            if (candidatos_df[i].votos > maior_dd)
            {
                maior_dd = candidatos_df[i].votos;
                vencedor_dd = candidatos_df[i].get_nome_urna_candidato();
            }
        }
    }
    cout << "\n\n\n";
    cout << "Vencedor Presidente: " << vencedor_br << endl;
    cout << "Vencedor Governador: " << vencedor_gov << endl;
    cout << "Vencedor Senador: " << vencedor_s1 << endl;
    cout << "Vencedor Deputado Federal: " << vencedor_df << endl;
    cout << "Vencedor Deputado Distrital: " << vencedor_dd << endl;
}
