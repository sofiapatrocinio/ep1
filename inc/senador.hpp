#ifndef SENADOR_HPP
#define SENADOR_HPP
#include "candidato.hpp"
#include <string>

class Senador: public Candidato{
    private:
    string suplente1;
    string suplente2;
    public:
    int votos;
    Senador(string,string,string,string,string,string);
    ~Senador();
    string get_suplente1();
    void set_suplente1(string suplente1);
    string get_suplente2();
    void set_suplente2(string suplente2);
};

#endif
