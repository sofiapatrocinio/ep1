#ifndef PRESIDENTE_HPP
#define PRESIDENTE_HPP
#include "candidato.hpp"
#include <string>

class Presidente: public Candidato{
    private:
    string vice;
    public:
    int votos;
    Presidente(string,string,string,string,string,string);
    ~Presidente();
    string get_vice();
    void set_vice(string vice);
};

#endif
