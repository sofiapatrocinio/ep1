#ifndef LEITURA_HPP
#define LEITURA_HPP
#include <string>
#include <vector>
#include "candidato.hpp"
#include "presidente.hpp"
#include "governador.hpp"
#include "senador.hpp"
using namespace std;

class Leitura{
     
    
 public:
     Leitura();
    ~Leitura();
  vector<Candidato> armazenar_candidatosdf();
  vector<Presidente> armazenar_candidatosbr();
  vector<Governador> armazenar_governadores();
  vector<Senador> armazenar_senadores();

 
};

#endif