#ifndef ELEITOR_HPP
#define ELEITOR_HPP
#include <string>

using namespace std;

class Eleitor{
    private:
    string nome_eleitor;
    string voto_presidente;
    string voto_governador;
    string voto_senador1;
    string voto_senador2;
    string voto_depu_federal;
    string voto_depu_distrital;
    public:
    Eleitor(string, string, string, string, string, string, string);
	~Eleitor();
	void set_nome_eleitor(string);
	string get_nome_eleitor();
	void set_voto_presidente(string);
	string get_voto_presidente();
	void set_voto_senador1(string);
	string get_voto_senador1();
    void set_voto_senador2(string);
	string get_voto_senador2();
	void set_voto_governador(string);
	string get_voto_governador();
	void set_voto_depu_federal(string);
	string get_voto_depu_federal();
	void set_voto_depu_distrital(string);
	string get_voto_depu_distrital();

};

#endif