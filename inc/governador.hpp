#ifndef GOVERNADOR_HPP
#define GOVERNADOR_HPP
#include "candidato.hpp"
#include <string>

class Governador: public Candidato{
    private:
    string vice;
    public:
    int votos;
    Governador(string,string,string,string,string,string);
    ~Governador();
    string get_vice();
    void set_vice(string vice);
};

#endif
