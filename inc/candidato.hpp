#ifndef CANDIDATO_HPP
#define CANDIDATO_HPP
#include <string>
using namespace std;

class Candidato{
    private:
      
  string descricao_cargo;
  string numero_candidato;
  string nome_urna_candidato;
  string numero_partido;
  string sigla_partido;
  string nome_partido;

  
    
 public:
    int votos;
    Candidato();
     Candidato(string descricao_cargo,string numero_candidato,string nome_urna_candidato,string numero_partido,string sigla_partido,string nome_partido);
    ~Candidato();
  string get_descricao_cargo();
  void set_descricao_cargo(string descricao_cargo);
  string get_numero_candidato();
  void set_numero_candidato(string numero_candidato);
  string get_nome_urna_candidato();
  void set_nome_urna_candidato(string nome_urna_candidato);
  string get_numero_partido();
  void set_numero_partido(string numero_partido);
  string get_sigla_partido();
  void set_sigla_partido(string sigla_partido);
  string get_nome_partido();
  void set_nome_partido(string nome_partido);


 
};

#endif