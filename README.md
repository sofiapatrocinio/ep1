# Exercício de Programação 1

Acesse a descrição completa deste exercício na [wiki](https://gitlab.com/oofga/eps_2018_2/ep1/wikis/Descricao)

## Como usar o projeto
* Execute o comando:

```sh
git clone https://gitlab.com/sofiapatrocinio/ep1.git
```
* Compile o projeto com o comando:

```sh
make
```

* Execute o projeto com o comando:

```sh
make run
```
## Funcionalidades do projeto
- Inserir número de urna de cada candidato
- Corrigir o número
- Votar em branco e nulo
- Confirmar
- Relatorio da relação do eleitor com seus votos
- Vencedor de cada cargo

## Bugs e problemas
- Relatório não printa BRANCO e NULO

## Referências
O projeto contém seis classes, sendo elas: candidato, eleitor, leitura, governador, senador e presidente.

<b>Classe Candidato:</b>

Contém sete atributos: Descrição Cargo, Número do Candidato na Urna, Nome do Candidato da Urna, Número do Partido, Sigla do Partido e Nome do Partido em que faz parte e um contador de votos.

Essa classe serve de base para três classes: governador, senador e presidente.

<b>Classe Eleitor:</b>

Eleitor possui sete atributos: Nome, Voto para Presidente, Voto para Governador, Voto para Senador 1, Voto para Senador 2, Voto para Deputado Federal e Voto para Deputado Distrital.

É chamada na main como um vetor, onde armazena cada voto de eleitor.

<b>Classe Leitura:</b>

Não possui atributos, mas quatro métodos em que armazena dados de candidatos DF, que corresponde apenas a deputados distritais e federais, dados de candidatos BR, trata-se de presidentes, dados de governadores e senadores.

A diferença entre os métodos se dá devido a presidente e governador possuir vice e senador possuir primeiro e segundo suplente.

<b>Classe Governador:</b>

Possui atributo vice, que corresponde ao vice governador.

Classe herdada de Candidato.

<b>Classe Senador:</b>

Possui atributos suplente1 e suplente2, que corresponde aos suplentes que poderão assumir cargo em seu lugar.

Classe herdada de Candidato.

<b>Classe Presidente:</b>

Possui atributo vice, que corresponde ao vice presidente.

Classe herdada de Candidato.

